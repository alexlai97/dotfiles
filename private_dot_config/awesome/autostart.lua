local naughty = require("naughty")
local awful = require("awful")
require("variables")

--  {#{ examples for spawn callback
-- reference https://awesomewm.org/doc/api/libraries/awful.spawn.html
--
-- local noisy = [[bash -c '
--   for I in $(seq 1 5); do
--     date
--     echo err >&2
--     sleep 2
--   done
-- ']]

-- awful.spawn.with_line_callback(noisy, {
--     stdout = function(line)
--         naughty.notify { text = "LINE:"..line }
--     end,
--     stderr = function(line)
--         naughty.notify { text = "ERR:"..line}
--     end,
-- })
--  }#}


--{#{ programs to spawn on start
-- awful.spawn.once(scripts_path .. "weather show")

-- tools
awful.spawn.single_instance("fcitx5")
awful.spawn.once("libinput-gestures-setup start")
awful.spawn.once(home_path .. ".fehbg")
awful.spawn.single_instance("xss-lock -- " .. lockscreen_command)
awful.spawn.single_instance("picom -b")
awful.spawn.single_instance("thunar --daemon")
awful.util.spawn_with_shell("pgrep volumeicon || volumeicon &")
awful.util.spawn_with_shell("pgrep cfw || cfw --enable-crashpad")
awful.util.spawn_with_shell("pgrep nm-applet || nm-applet")

awful.util.spawn_with_shell("pgrep nextcloud || nextcloud --background")
-- awful.spawn.raise_or_spawn("1password")

-- chat
-- awful.spawn.single_instance("telegram-desktop")
awful.util.spawn_with_shell("pgrep feishu || feishu")

-- notes
awful.util.spawn_with_shell("pgrep notion-app || notion-app")
awful.util.spawn_with_shell("pgrep obsidan || obsidan")

--}#}
