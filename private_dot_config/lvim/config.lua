-- LVIM https://github.com/LunarVim/LunarVim
-- endless configuration: https://github.com/abzcoding/lvim

-- TODO:
-- [ ] add multi cursor
-- [ ] add opt for tabnine (can do lvim.builtin.cmp_tabinine.active = false)
-- [ ] https://github.com/folke/persistence.nvim

-- gui
vim.opt.guifont = "FiraCode Nerd Font:h9" -- for gui nvim clients
vim.g.neovide_cursor_vfx_mode = "railgun"

-- general
vim.log.level = "warn"
lvim.colorscheme = "onedarker"
vim.opt.clipboard = "unnamedplus" -- allows neovim to access the system clipboard
lvim.format_on_save = false
-- vim.cmd [[set runtimepath+=/usr/share/vim/vimfiles]]

vim.opt.spelllang = "en,cjk"

-- leader keys
lvim.leader = "space"
vim.g.maplocalleader = ','

-- foldmethod
vim.wo.foldmethod = "marker"
vim.wo.foldmarker = "{#{,}#}"

-- plugins
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "startify"
-- lvim.builtin.notify.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true
lvim.builtin.dap.active = true

-- TODO: move them to elsewhere
-- {#{
-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
  },
  -- for normal mode
  n = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
  },
}

lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "go",
  "gomod",
  "comment",
  "fish",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
  "toml",
  "dockerfile",
  "markdown",
}
lvim.builtin.treesitter.highlight.enabled = true
lvim.builtin.treesitter.rainbow = {
  enable = true,
  colors = {
    "Gold",
    "Orchid",
    "DodgerBlue",
    "Cornsilk",
    "Salmon",
    "LawnGreen",
  },
  disable = { "html" },
}
-- }#}

-- require user configs
require("user.plugins").config()
require("user.dap").config()
require("user.keymappings").config()
require("user.lint-and-format").config()
