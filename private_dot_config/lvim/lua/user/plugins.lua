local M = {}
M.config = function()
  lvim.plugins = {
    { "tpope/vim-surround" },
    { "tpope/vim-fugitive" },
    { "tpope/vim-repeat" },
    { "ggandor/lightspeed.nvim", config = true, },
    { "ziontee113/syntax-tree-surfer" },
    { "folke/trouble.nvim", cmd = "TroubleToggle", },
    { "ray-x/lsp_signature.nvim",
      event = "BufRead",
      config = true,
    },
    { "aserowy/tmux.nvim",
      config = function()
        require("tmux").setup({
          copy_sync = {
            enable = false,
            sync_clipbard = false,
            sync_delete = false,
            sync_unnamed = false,
          },
          navigation = {
            enable_default_keybindings = true,
          },
          resize = {
            enable_default_keybindings = true,
          },
        })
      end
    },
    {
      "folke/todo-comments.nvim",
      dependencies = "nvim-lua/plenary.nvim",
      config = true
    },
    {
      "folke/persistence.nvim",
      event = "BufReadPre", -- this will only start session saving when an actual file was opened
      config = true
    },
    {
      "tzachar/cmp-tabnine",
      build = "./install.sh",
      dependencies = "hrsh7th/nvim-cmp",
      event = "InsertEnter",
    },
    { "hrsh7th/cmp-emoji" },
    { "hrsh7th/cmp-calc" },
    { "simrat39/symbols-outline.nvim", cmd = "SymbolsOutline", config = true },
    { "rafcamlet/nvim-luapad", cmd = { "Luapad" } },
    { "metakirby5/codi.vim", cmd = { "Codi", "CodiNew" } },
    -- { 'lervag/vimtex', config = function()
    -- end,
    -- },

    -- {#{ dap related
    -- { "rcarriga/nvim-dap-ui", dependencies = { "mfussenegger/nvim-dap" },
    --   config = function() require("dapui").setup() end
    -- },
    { "theHamsta/nvim-dap-virtual-text", dependencies = { "mfussenegger/nvim-dap" }, 
      config = true , 
    },
    { "leoluz/nvim-dap-go", config = true },
    -- }#}

    -- {#{ aesthetics
    -- { "p00f/nvim-ts-rainbow" },
    -- { "folke/tokyonight.nvim" },
    { "stevearc/dressing.nvim" },
    { "folke/zen-mode.nvim",
      config = function()
        require("zen-mode").setup {
          window = {
            options = {
              number = true,
            }
          },
          plugins = {
            gitsigns = { enabled = true },
            tmux = { enabled = true },
          }
        }
      end
    },
    { "folke/twilight.nvim",
      config = function()
        require("twilight").setup {
          expand = {
            "function",
            "method",
            "table",
            -- "if_statement",
          },
        }
      end
    }
  }
  -- }#}
end

return M
