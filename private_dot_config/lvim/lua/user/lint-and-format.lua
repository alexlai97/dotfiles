local M = {}
M.config = function()

  local formatters = require "lvim.lsp.null-ls.formatters"
  formatters.setup {
    { command = "black",
      filetypes = { "python" },
    },
    { command = "yamlfmt",
      filetypes = { "yaml" },
    },
    {
      command = "prettier",
      args = { "--print-width", "100" },
      filetypes = { "typescript", "typescriptreact" },
    },
  }

  local linters = require "lvim.lsp.null-ls.linters"
  linters.setup {
    { command = "flake8",
      filetypes = { "python" },
    },
    { command = "yamllint",
      filetypes = { "yaml" },
    },
    {
      command = "shellcheck",
      args = { "--severity", "warning" },
    },
    -- {
    --   command = "codespell",
    --   filetypes = { "javascript", "python" },
    -- },
  }

  -- local code_actions = require "lvim.lsp.null-ls.code_actions"
  -- code_actions.setup {
  --   {
  --     command = "proselint"
  --   },
  -- }
end

return M
