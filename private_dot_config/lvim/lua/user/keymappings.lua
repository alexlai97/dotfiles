local M = {}
M.config = function()
  --{#{ starting with <Space>
  lvim.builtin.which_key.vmappings = {}
  lvim.builtin.which_key.mappings = {
    -- ["w"] = { "<cmd>w!<CR>", "Save" },
    -- ["q"] = { "<cmd>q!<CR>", "Quit" },
    -- ["/"] = { "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", "Comment" },
    ["x"] = { "<cmd>BufferKill<CR>", "Close Buffer" },
    [">"] = { "<cmd>Telescope buffers<cr>", "Find buffer" },
    -- ["e"] = { "<cmd>NvimTreeToggle<CR>", "Toggle NvimTree" },
    ["/"] = { "<cmd>Telescope live_grep<cr>", "Live Grep" },
    [";"] = { "<cmd>Alpha<CR>", "Dashboard" },
    -- avoid being overwritten by dap keybindings
    ["dU"] = { '<cmd>lua require("dapui").toggle()<CR>', "Toggle dapui" },
    ["dTg"] = { '<cmd>lua require("dap-go").debug_test()<CR>', "Debug go test" },
    ["Z"] = { '<cmd>lua require("zen-mode").toggle()<CR>', "ZenMode" },
    f = {
      name = "Find",
      f = { require("lvim.core.telescope.custom-finders").find_project_files, "Find File" },
      -- f = { "<cmd>Telescope find_files<cr>", "Find File" },
      r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
    },
    h = {
      name = "Help",
      M = { "<cmd>Telescope man_pages<cr>", "Man Pages" },
      k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
      C = { "<cmd>Telescope commands<cr>", "Commands" },
      R = { "<cmd>Telescope registers<cr>", "Registers" },
      h = { "<cmd>Telescope help_tags<cr>", "Find Help" },
    },
    t = {
      name = "toggle/switch",
      b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      c = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
      C = {
        "<cmd>lua require('telescope.builtin').colorscheme({enable_preview = true})<cr>",
        "Colorscheme with Preview",
      },
      n = { "<cmd>set number!<cr>", "Toggle Number" },
      s = { "<cmd>SymbolsOutline<cr>", "Toggle SymbolsOutline" },
      t = { "<cmd>TodoTrouble<cr>", "Toggle Todo Trouble" },
      T = { "<cmd>TroubleToggle<cr>", "Toggle Trouble" },
    },
    s = {
      name = "session",
      s = { '<cmd>lua require("persistence").load()<cr>', "Restore session for cwd"},
      l = { '<cmd>lua require("persistence").load({ last = true })<cr>', "Restore last session"},
      d = { '<cmd>lua require("persistence").stop()<cr>', "Stop persistence"},
    },
    b = {
      name = "Buffers",
      j = { "<cmd>BufferLinePick<cr>", "Jump" },
      f = { "<cmd>Telescope buffers<cr>", "Find" },
      b = { "<cmd>BufferLineCyclePrev<cr>", "Previous" },
      -- w = { "<cmd>BufferWipeout<cr>", "Wipeout" }, -- TODO: implement this for bufferline
      e = {
        "<cmd>BufferLinePickClose<cr>",
        "Pick which buffer to close",
      },
      h = { "<cmd>BufferLineCloseLeft<cr>", "Close all to the left" },
      l = {
        "<cmd>BufferLineCloseRight<cr>",
        "Close all to the right",
      },
      D = {
        "<cmd>BufferLineSortByDirectory<cr>",
        "Sort by directory",
      },
      L = {
        "<cmd>BufferLineSortByExtension<cr>",
        "Sort by language",
      },
    },
    p = {
      name = "Plugins",
      i = { "<cmd>Lazy install<cr>", "Install" },
      s = { "<cmd>Lazy sync<cr>", "Sync" },
      S = { "<cmd>Lazy clear<cr>", "Status" },
      c = { "<cmd>Lazy clean<cr>", "Clean" },
      u = { "<cmd>Lazy update<cr>", "Update" },
      p = { "<cmd>Lazy profile<cr>", "Profile" },
      l = { "<cmd>Lazy log<cr>", "Log" },
      d = { "<cmd>Lazy debug<cr>", "Debug" },
    },
    g = {
      name = "Git",
      g = { "<cmd>lua require 'lvim.core.terminal'.lazygit_toggle()<cr>", "Lazygit" },
      j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
      k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
      l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
      p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
      r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
      R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
      s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
      u = {
        "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
        "Undo Stage Hunk",
      },
      o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
      b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
      C = {
        "<cmd>Telescope git_bcommits<cr>",
        "Checkout commit(for current file)",
      },
      d = {
        "<cmd>Gitsigns diffthis HEAD<cr>",
        "Git Diff",
      },
    },
    l = {
      name = "LSP",
      -- a = { "<cmd>lua require('lvim.core.telescope').code_actions()<cr>", "Code Action" },
      a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
      d = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer Diagnostics" },
      w = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
      f = { "<cmd>lua require('lvim.lsp.utils').format()<cr>", "Format" },
      i = { "<cmd>LspInfo<cr>", "Info" },
      I = { "<cmd>Mason<cr>", "Mason Info" },
      j = {
        "<cmd>lua vim.diagnostic.goto_next()<cr>",
        "Next Diagnostic",
      },
      k = {
        "<cmd>lua vim.diagnostic.goto_prev()<cr>",
        "Prev Diagnostic",
      },
      l = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
      q = { "<cmd>lua vim.diagnostic.setloclist()<cr>", "Quickfix" },
      r = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Rename" },
      s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
      S = {
        "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
        "Workspace Symbols",
      },
      e = { "<cmd>Telescope quickfix<cr>", "Telescope Quickfix" },
    },
      L = {
        name = "+LunarVim",
        c = {
          "<cmd>edit " .. get_config_dir() .. "/config.lua<cr>",
          "Edit config.lua",
        },
        d = { "<cmd>LvimDocs<cr>", "View LunarVim's docs" },
        f = {
          "<cmd>lua require('lvim.core.telescope.custom-finders').find_lunarvim_files()<cr>",
          "Find LunarVim files",
        },
        g = {
          "<cmd>lua require('lvim.core.telescope.custom-finders').grep_lunarvim_files()<cr>",
          "Grep LunarVim files",
        },
        k = { "<cmd>Telescope keymaps<cr>", "View LunarVim's keymappings" },
        i = {
          "<cmd>lua require('lvim.core.info').toggle_popup(vim.bo.filetype)<cr>",
          "Toggle LunarVim Info",
        },
        I = {
          "<cmd>lua require('lvim.core.telescope.custom-finders').view_lunarvim_changelog()<cr>",
          "View LunarVim's changelog",
        },
        l = {
          name = "+logs",
          d = {
            "<cmd>lua require('lvim.core.terminal').toggle_log_view(require('lvim.core.log').get_path())<cr>",
            "view default log",
          },
          D = {
            "<cmd>lua vim.fn.execute('edit ' .. require('lvim.core.log').get_path())<cr>",
            "Open the default logfile",
          },
          l = {
            "<cmd>lua require('lvim.core.terminal').toggle_log_view(vim.lsp.get_log_path())<cr>",
            "view lsp log",
          },
          L = { "<cmd>lua vim.fn.execute('edit ' .. vim.lsp.get_log_path())<cr>", "Open the LSP logfile" },
          n = {
            "<cmd>lua require('lvim.core.terminal').toggle_log_view(os.getenv('NVIM_LOG_FILE'))<cr>",
            "view neovim log",
          },
          N = { "<cmd>edit $NVIM_LOG_FILE<cr>", "Open the Neovim logfile" },
        },
        r = { "<cmd>LvimReload<cr>", "Reload LunarVim's configuration" },
        u = { "<cmd>LvimUpdate<cr>", "Update LunarVim" },
      },
    --{#{ cr "+rc edit misc config files"
    ["cr"] = {
      name = "+rc edit misc config files",
      t = { ":edit ~/.tmux.conf<cr>", "tmux.conf" },
      l = {
        name = "+lvim",
        c = {
          "<cmd>edit " .. get_config_dir() .. "/config.lua<cr>",
          "Edit config.lua",
        },
        f = {
          "<cmd>lua require('user.telescope.custom-finders').find_lvim_config_files()<cr>",
          "Find ~/.config/lvim/ files",
        },
        g = {
          "<cmd>lua require('user.telescope.custom-finders').grep_lvim_config_files()<cr>",
          "Grep ~/.config/lvim files",
        },
        F = {
          "<cmd>lua require('lvim.core.telescope.custom-finders').find_lunarvim_files()<cr>",
          "Find ~/.local/share/lunarvim/ files",
        },
        G = {
          "<cmd>lua require('lvim.core.telescope.custom-finders').grep_lunarvim_files()<cr>",
          "Grep ~/.local/share/lunarvim/ files",
        },
      },
      a = { ":edit ~/.config/alacritty/alacritty.yml<cr>", "alacritty/alacritty.yml" },
      A = {
        name = "+awesome",
        c = { ":edit ~/.config/awesome/rc.lua<cr>", "rc.lua" },
        s = { ":edit ~/.config/awesome/autostart.lua<cr>", "autostart.lua" },
        v = { ":edit ~/.config/awesome/variables.lua<cr>", "variables.lua" },
        r = { ":edit ~/.config/awesome/rules.lua<cr>", "rules.lua" },
        m = { ":edit ~/.config/awesome/start_menu.lua<cr>", "start_menu.lua" },
        b = { ":edit ~/.config/awesome/wibar.lua<cr>", "wibar.lua" },
        k = { ":edit ~/.config/awesome/keybindings.lua<cr>", "keybinding.lua" },
      },
      f = { ":edit ~/.config/fish/config.fish<cr>", "fish/config.fish" },
    },
    --}#}
    --{#{ "+cd change directory"
    ["cd"] = {
      name = "+cd change directory",
      d = { ":cd %:p:h<CR>", "cd for the environment" },
      l = { ":lcd %:p:h<CR>", "cd for current window" },
      t = { ":tcd %:p:h<CR>", "cd for current tab and window" },
    },
    --}#}
  }
  --}#}

  -- normal mode
  lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
  -- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

  lvim.builtin.which_key.on_config_done = function(wk)
    -- normal mode (not starting with leaderkey)
    wk.register({
      ["<localleader>"] = {},
      --{#{ buffer next, prev
      ["L"] = { "<cmd>bnext<cr>", "next buffer" },
      ["H"] = { "<cmd>bprev<cr>", "next buffer" },
      --}#}
      --{#{ <F2> lsp: Rename Symbol/Word
      ["<F2>"] = { vim.lsp.buf.rename, "Rename current symbol/word" },
      --}#}
      --{#{ <F3> toggle nvim-tree
      ["<F3>"] = { "<cmd>NvimTreeToggle<CR>", "Toggle NvimTree" },
      --}#}
      -- {#{ F5,F10,F11,F12 dap continue,step_over,into,out
      ["<F5>"] = { "<cmd>lua require'dap'.continue()<CR>", "dap continue" },
      ["<F10>"] = { "<cmd>lua require'dap'.step_over()<CR>", "dap step over" },
      ["<F11>"] = { "<cmd>lua require'dap'.step_into()<CR>", "dap step into" },
      ["<F12>"] = { "<cmd>lua require'dap'.step_out()<CR>", "dap step out" },
      -- }#}
      --{#{ syntax-tree-surfer Move object up/down; Select
      ["vu"] = { '<cmd>lua require("syntax-tree-surfer").move("n", true)<cr>', "Move object up" },
      ["vd"] = { '<cmd>lua require("syntax-tree-surfer").move("n", false)<cr>', "Move object down" },
      ["vx"] = { '<cmd>lua require("syntax-tree-surfer").select()<cr>', "Select Object" },
      ["vn"] = { '<cmd>lua require("syntax-tree-surfer").select_current_node()<cr>', "Select Current Node" },
      --}#}
    }, { mode = "n", silent = false, noremap = true })

    wk.register({
      -- ["p"] = { '"_dp', "paste with last register" }
    }, { mode = "v", silent = false, noremap = true })

    -- visual block mode
    wk.register({
      --{#{ syntax-tree-surfer Surf Prev/Next/Child/Parent; Move Forward/Backward
      ["K"] = { '<cmd>lua require("syntax-tree-surfer").surf("prev", "visual")<cr>', "Surf Prev" },
      ["J"] = { '<cmd>lua require("syntax-tree-surfer").surf("next", "visual")<cr>', "Surf Next" },
      ["L"] = { '<cmd>lua require("syntax-tree-surfer").surf("child", "visual")<cr>', "Surf Child" },
      ["H"] = { '<cmd>lua require("syntax-tree-surfer").surf("parent", "visual")<cr>', "Surf Parent" },
      ["gj"] = { '<cmd>lua require("syntax-tree-surfer").surf("next", "visual", true)<cr>', "Move Forward" },
      ["gk"] = { '<cmd>lua require("syntax-tree-surfer").surf("prev", "visual", true)<cr>', "Move Backward" },
      --}#}
    }, { mode = "x", silent = false, noremap = true })
  end

end

return M
