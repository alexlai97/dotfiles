local M = {}

local _, builtin = pcall(require, "telescope.builtin")
-- local _, finders = pcall(require, "telescope.finders")
-- local _, pickers = pcall(require, "telescope.pickers")
-- local _, sorters = pcall(require, "telescope.sorters")
local _, themes = pcall(require, "telescope.themes")
-- local _, actions = pcall(require, "telescope.actions")
-- local _, previewers = pcall(require, "telescope.previewers")
-- local _, make_entry = pcall(require, "telescope.make_entry")

-- local utils = require "lvim.utils"

function M.find_lvim_config_files(opts)
  opts = opts or {}
  local theme_opts = themes.get_ivy {
    sorting_strategy = "ascending",
    layout_strategy = "bottom_pane",
    prompt_prefix = ">> ",
    prompt_title = "~ Lvim files ~",
    cwd = get_runtime_dir(),
    search_dirs = { get_config_dir() },
  }
  opts = vim.tbl_deep_extend("force", theme_opts, opts)
  builtin.find_files(opts)
end

function M.grep_lvim_config_files(opts)
  opts = opts or {}
  local theme_opts = themes.get_ivy {
    sorting_strategy = "ascending",
    layout_strategy = "bottom_pane",
    prompt_prefix = ">> ",
    prompt_title = "~ search Lvim ~",
    cwd = get_runtime_dir(),
    search_dirs = { get_config_dir() },
  }
  opts = vim.tbl_deep_extend("force", theme_opts, opts)
  builtin.live_grep(opts)
end

return M
