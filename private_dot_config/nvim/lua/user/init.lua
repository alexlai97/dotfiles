local config = {

  -- Disable default plugins
  enabled = {
    bufferline = true,
    neo_tree = true,
    lualine = true,
    gitsigns = true,
    colorizer = true,
    toggle_term = true,
    comment = true,
    symbols_outline = true,
    indent_blankline = true,
    dashboard = true,
    which_key = true,
    neoscroll = false,
    ts_rainbow = true,
    ts_autotag = true,
  },

  -- Disable AstroNvim ui features
  ui = {
    nui_input = true,
    telescope_select = true,
  },

  -- Configure plugins
  plugins = {
    -- Add plugins, the packer syntax without the "use"
    init = {
      { "tpope/vim-surround" },
      { "tpope/vim-fugitive" },
      { "tpope/vim-repeat" },
      { "ziontee113/syntax-tree-surfer" },
      { "bkad/CamelCaseMotion" },
      {
        "ggandor/lightspeed.nvim",
        config = function()
          require("lightspeed").setup {}
        end,
      },
      {
        "ray-x/lsp_signature.nvim",
        event = "BufRead",
        config = function()
          require("lsp_signature").setup()
        end,
      },
      {
        "rcarriga/nvim-dap-ui",
        requires = {"mfussenegger/nvim-dap"}
      },
      {
        "aserowy/tmux.nvim",
        config = function ()
          require("tmux").setup({
            copy_sync = {
              enable = true,
            },
            navigation = {
              enable_default_keybindings = true,
            },
            resize = {
              enable_default_keybindings = true,
            },
          })
        end
      }
    },
    -- All other entries override the setup() call for default plugins
    treesitter = {
      ensure_installed = { "lua" },
    },
    packer = {
      compile_path = vim.fn.stdpath "config" .. "/lua/packer_compiled.lua",
    },
  },

  -- Add paths for including more VS Code style snippets in luasnip
  luasnip = {
    vscode_snippet_paths = {},
  },

  -- Modify which-key registration
  ["which-key"] = {
    -- Add bindings
    register_mappings = {
      -- first key is the mode, n == normal mode
      n = {
        -- second key is the prefix, <leader> prefixes
        ["<leader>"] = {
          -- which-key registration table for normal mode, leader prefix
          -- ["N"] = { "<cmd>tabnew<cr>", "New Buffer" },
        },
        ["<localleader>"] = {
          --{#{ <localleader>cd "+cd change directory"
          ["cd"] = {
            name = "+cd change directory",
            d = { ":cd %:p:h<CR>", "cd for the environment" },
            l = { ":lcd %:p:h<CR>", "cd for current window" },
            t = { ":tcd %:p:h<CR>", "cd for current tab and window" },
          },
          --}#}
          --{#{ <localleader>rc "+rc edit misc config files"
          ["rc"] = {
            name = "+rc edit misc config files",
            t = {":edit ~/.tmux.conf<cr>", "tmux.conf"},
            n = {
              name = "+nvim",
              u = {":edit ~/.config/nvim/lua/user/init.lua<cr>", "lua/user/init.lua"},
            },
            a = {":edit ~/.config/alacritty/alacritty.yml<cr>", "alacritty/alacritty.yml"},
            A = {
              name = "+awesome",
              c = {":edit ~/.config/awesome/rc.lua<cr>", "rc.lua"},
              s = {":edit ~/.config/awesome/autostart.lua<cr>", "autostart.lua"},
              v = {":edit ~/.config/awesome/variables.lua<cr>", "variables.lua"},
              r = {":edit ~/.config/awesome/rules.lua<cr>", "rules.lua"},
              m = {":edit ~/.config/awesome/start_menu.lua<cr>", "start_menu.lua"},
              b = {":edit ~/.config/awesome/wibar.lua<cr>", "wibar.lua"},
              k = {":edit ~/.config/awesome/keybindings.lua<cr>", "keybinding.lua"},
            },
            f = {":edit ~/.config/fish/config.fish<cr>", "fish/config.fish"},
          },
        --}#}
        },
        --{#{ buffer next/prev
        ["[b"] = { ":bprevious<CR>", "Switch to previous buffer"},
        ["]b"] = { ":bnext<CR>", "Switch to next buffer"},
        --}#}
        --{#{ syntax-tree-surfer Move object up/down; Select
        ["vu"]= {'<cmd>lua require("syntax-tree-surfer").move("n", true)<cr>', "Move object up" },
        ["vd"] = { '<cmd>lua require("syntax-tree-surfer").move("n", false)<cr>', "Move object down"},
        ["vx"]={ '<cmd>lua require("syntax-tree-surfer").select()<cr>', "Select Object"},
        ["vn"]={ '<cmd>lua require("syntax-tree-surfer").select_current_node()<cr>' , "Select Current Node"},
        --}#}
        --{#{ <F2> lsp: Rename Symbol/Word
        ["<F2>"] = {vim.lsp.buf.rename, "Rename current symbol/word"},
        --}#}
        --{#{ <F3> toggle nvim-tree
        ["<F3>"] = {"<cmd>Neotree toggle<CR>", "Toggle Neo-tree"},
        --}#}
      },
      v = {
      },
      x = {
        --{#{ syntax-tree-surfer Surf Prev/Next/Child/Parent; Move Forward/Backward
        ["K"]={ '<cmd>lua require("syntax-tree-surfer").surf("prev", "visual")<cr>' , "Surf Prev"},
        ["J"]={ '<cmd>lua require("syntax-tree-surfer").surf("next", "visual")<cr>' , "Surf Next"},
        ["L"]={ '<cmd>lua require("syntax-tree-surfer").surf("child", "visual")<cr>' , "Surf Child"},
        ["H"]={ '<cmd>lua require("syntax-tree-surfer").surf("parent", "visual")<cr>' , "Surf Parent"},
        ["<A-j>"]= { '<cmd>lua require("syntax-tree-surfer").surf("next", "visual", true)<cr>' , "Move Forward"},
        ["<A-k>"]= {'<cmd>lua require("syntax-tree-surfer").surf("prev", "visual", true)<cr>' , "Move Backward"},
        --}#}
      }
    },
  },

  -- CMP Source Priorities
  -- modify here the priorities of default cmp sources
  -- higher value == higher priority
  -- The value can also be set to a boolean for disabling default sources:
  -- false == disabled
  -- true == 1000
  cmp = {
    source_priority = {
      nvim_lsp = 1000,
      luasnip = 750,
      buffer = 500,
      path = 250,
    },
  },

  -- Extend LSP configuration
  lsp = {
    -- add to the server on_attach function
    -- on_attach = function(client, bufnr)
    -- end,

    -- override the lsp installer server-registration function
    -- server_registration = function(server, opts)
    --   server:setup(opts)
    -- end

    -- Add overrides for LSP server settings, the keys are the name of the server
    ["server-settings"] = {
      -- example for addings schemas to yamlls
      -- yamlls = {
      --   settings = {
      --     yaml = {
      --       schemas = {
      --         ["http://json.schemastore.org/github-workflow"] = ".github/workflows/*.{yml,yaml}",
      --         ["http://json.schemastore.org/github-action"] = ".github/action.{yml,yaml}",
      --         ["http://json.schemastore.org/ansible-stable-2.9"] = "roles/tasks/*.{yml,yaml}",
      --       },
      --     },
      --   },
      -- },
    },
  },

  -- Diagnostics configuration (for vim.diagnostics.config({}))
  diagnostics = {
    virtual_text = true,
    underline = true,
  },

  -- null-ls configuration
  ["null-ls"] = function()
    -- Formatting and linting
    -- https://github.com/jose-elias-alvarez/null-ls.nvim
    local status_ok, null_ls = pcall(require, "null-ls")
    if not status_ok then
      return
    end

    -- Check supported formatters
    -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
    local formatting = null_ls.builtins.formatting

    -- Check supported linters
    -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
    local diagnostics = null_ls.builtins.diagnostics

    null_ls.setup {
      debug = false,
      sources = {
        -- Set a formatter
        formatting.rufo,
        -- Set a linter
        diagnostics.rubocop,
      },
      -- NOTE: You can remove this on attach function to disable format on save
      on_attach = function(client)
        if client.resolved_capabilities.document_formatting then
          vim.api.nvim_create_autocmd("BufWritePre", {
            desc = "Auto format before save",
            pattern = "<buffer>",
            callback = vim.lsp.buf.formatting_sync,
          })
        end
      end,
    }
  end,

  -- This function is run last
  -- good place to configure mappings and vim options
  polish = function()
    local map = vim.keymap.set
    local set = vim.opt

    vim.g.maplocalleader = ','
    set.relativenumber = true

    -- foldmethod
    vim.wo.foldmethod = "marker"
    vim.wo.foldmarker = "{#{,}#}"

    -- Set key bindings
    map("n", "<C-s>", ":w!<CR>")

    -- Set autocommands
    vim.api.nvim_create_augroup("packer_conf", {})
    vim.api.nvim_create_autocmd("BufWritePost", {
      desc = "Sync packer after modifying plugins.lua",
      group = "packer_conf",
      pattern = "plugins.lua",
      command = "source <afile> | PackerSync",
    })

    -- Set up custom filetypes
    -- vim.filetype.add {
    --   extension = {
    --     foo = "fooscript",
    --   },
    --   filename = {
    --     ["Foofile"] = "fooscript",
    --   },
    --   pattern = {
    --     ["~/%.config/foo/.*"] = "fooscript",
    --   },
    -- }
  end,
}

return config
