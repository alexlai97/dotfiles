function ssh-setup
    set -l host $argv[1]

    # Copy SSH public key to the host
    ssh-copy-id $host

    # Copy file to the host
    scp ~/.tmux.conf $host:~/.tmux.conf
end

complete -c ssh-setup -f -d Remote -xa "(__fish_complete_user_at_hosts)"
