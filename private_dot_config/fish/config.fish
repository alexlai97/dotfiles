if status is-login
  source ~/.env
end

if status is-interactive
  fzf_key_bindings # enable fzf functions in fish
  # source /opt/miniconda3/etc/fish/conf.d/conda.fish
  # conda activate base
  # conda activate default
  # conda activate ai-python3.11
  starship init fish | source # starship
  direnv hook fish | source # direnv
end

#{#{ abbreviations/alias
# git
abbr -a -- g git 
abbr -a -- gp 'git push' 
abbr -a -- gaa 'git add --all' 
abbr -a -- gc 'git commit -v' 
abbr -a -- gco 'git checkout' 
abbr -a -- gst 'git status' 
abbr -a -- gl 'git pull' 

# utils
abbr -a -- cp2cb 'xclip -selection clipboard' 
abbr -a -- za zathura 
abbr -a -- zfd 'zenity --file-selection --directory' 
abbr -a -- vf vifm 
abbr -a -- h htop 
abbr -a -- cz chezmoi 
abbr -a -- lg lazygit 
abbr -a -- lzd lazydocker 

# kubectl
abbr -a -- k kubectl 
abbr -a -- ka 'kubectl apply' 
abbr -a -- ke 'kubectl edit' 
abbr -a -- kE 'kubectl exec' 
abbr -a -- kg 'kubectl get' 
abbr -a -- kl 'kubectl logs' 
abbr -a -- kd 'kubectl describe' 
abbr -a -- kD 'kubectl delete' 
abbr -a -- kc 'kubectl create' 
abbr -a -- Kc 'kubectl config use-context' 
abbr -a -- KC 'kubectl config view --minify' 
abbr -a -- KN "kubectl config set-context --current --namespace (kubectl get ns -oname | cut -d'/' -f2 | fzf)"


# tmux
abbr -a -- tn 'tmux new -s' 
abbr -a -- ta 'tmux attach' 

# vim
abbr -a -- v lvim 
abbr -a -- view 'nvim -R' 

# common
abbr -a -- cl clear 
abbr -a -- l 'exa -la' 
abbr -a -- ... 'cd ../../' 
abbr -a -- .... 'cd ../../../' 
abbr -a -- - 'cd -' 
abbr -a -- d cdh 
abbr -a -- p 'ping -c 10 archlinux.org' 
abbr -a -- md mkdir 
abbr -a -- rd rmdir 

# not frequently used
abbr -a -- startx 'startx ^ ~/.cache/xorg_startx_stderr.log > ~/.cache/xorg_startx_stdout.log' 
abbr -a -- lc lfcd 
abbr -a -- his history 
abbr -a -- info 'info --vi-keys' 
#}#}

#{#{ key bindings
bind -M insert \cf forward-char
#}#}




# vim: ft=fish
